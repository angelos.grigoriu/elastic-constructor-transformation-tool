const ConstructorIOClient = require("@constructor-io/constructorio-node");
const Papa = require("papaparse");
const elasticProducts = require("./elastic-products10000.json");
const FS = require("fs");
const {
  buildMetaData,
  buildFacetData,
} = require("./default-headers/defaultObject");

console.time("Start");

const restrictedFieldsForMetaData = [
  "_index",
  "name",
  "primary_keyword",
  "category_ids",
  "brand",
  "attributes",
  "_type",
  "contracted_product",
  "is_new_product",
];

// By removing line 29 we can deeply transform objects into separate metadata keys
// const recursiveTransform = (
//   productOrNestedField,
//   fieldsNotForTransformation,
//   transformedValues = {}
// ) => {
//   for (const elasticKey in productOrNestedField) {
//     if (!fieldsNotForTransformation.includes(elasticKey)) {
//       const elasticValue = productOrNestedField[elasticKey];
//       if (
//         !Array.isArray(elasticValue) &&
//         typeof elasticValue === "object" &&
//         elasticKey === "_source"
//       ) {
//         recursiveTransform(
//           elasticValue,
//           fieldsNotForTransformation,
//           transformedValues
//         );
//       } else if (
//         elasticValue !== null &&
//         (Array.isArray(elasticValue) || typeof elasticValue === "object")
//       ) {
//         transformedValues[`metadata:${elasticKey}`] =
//           JSON.stringify(elasticValue);
//       } else {
//         transformedValues[`metadata:${elasticKey}`] =
//           elasticValue !== null ? elasticValue : "";
//       }
//     }
//   }
//   return transformedValues;
// };

console.log("Starting attributes extraction...");

// const allAttributes = elasticProducts.reduce((acc, product) => {
//   const { attributes } = product._source;
//   const prefix = 'facet:';
//   if (attributes) {
//     attributes.forEach((attribute) => {
//       if (!acc.includes(prefix + attribute.label)) {
//         acc.push(prefix + attribute.label);
//       }
//     });
//   }
//   return acc;
// }, []);

// const allMetadataFields = elasticProducts.reduce((acc, product) => {});

console.log("Attributes extraction finished!");

// transform products into ConstructorIO format

const transformedProducts = [];

const buildProductData = (product) => {
  const transformedFacetData = buildFacetData(product);
  const transformedMetadata = buildMetaData(product);

  const transformedProduct = {
    ...transformedFacetData,
    ...transformedMetadata,
  };

  return transformedProduct;
};

console.log("Starting product transformation....");

elasticProducts.forEach((product) => {
  const {
    name,
    primary_keyword,
    category_ids,
    main_image = "",
    part,
  } = product._source;

  const productAttributesAndMetadata = buildProductData(product);

  const transformedProduct = {
    ...productAttributesAndMetadata,
    item_name: name,
    id: product._id,
    url: `https://www.cromwell.co.uk/${part}`,
    image_url: `https://static-content.cromwell.co.uk/images/854_854/${main_image}`,
    description: "Some product description",
    keywords: primary_keyword.join(" | "),
    group_ids: category_ids.join(" | "),
  };

  transformedProducts.push(transformedProduct);
});

console.log("Transforming products finished!");

// create csv file with transformed products

const csvFormat = Papa.unparse(transformedProducts);
const fileName = "transformed-products10000.csv";

FS.writeFile(`results/${fileName}`, csvFormat, (err) => {
  if (err) {
    console.log(
      "=============== Error writing products to CSV ==============="
    );
    console.log(err);
  } else {
    console.log("CSV file created successfully!");
    console.log("Starting upload...");

    // connection to Constructor
    const constructorio = new ConstructorIOClient({
      // apiKey: "" // STG
      apiKey: process.env.API_KEY,
      apiToken: process.env.API_TOKEN,
    });

    //     const updateCatalog = async (csvBuffer) => {
    //       try {
    //         constructorio.catalog.updateCatalog({
    //           section: "Products",
    //           notificationEmail: process.env.EMAIL,
    //           items: csvBuffer,
    //         });
    //       } catch (error) {
    //         console.error("Constructor connection error ");
    //         console.error(error);
    //       }
    //     };

    const replaceCatalog = async (csvBuffer) => {
      try {
        constructorio.catalog.replaceCatalog({
          section: "Products",
          notificationEmail: process.env.EMAIL,
          items: csvBuffer,
          force: true,
        });
      } catch (error) {
        console.error("Constructor connection error ");
        console.error(error);
      }
    };

    FS.readFile(`./results/${fileName}`, (err, data) => {
      if (err) {
        console.log("Error reading the file >>>>>>>>", err);
        return;
      }

      replaceCatalog(data)
        .then(() => {
          console.log("Successfully uploaded CSV");
        })
        .catch((err) => {
          console.error("Error uploading CSV");
          console.log(err);
        });
    });
  }
});

console.timeEnd("Start");
