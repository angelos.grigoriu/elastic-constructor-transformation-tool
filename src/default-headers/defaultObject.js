const { allAttributes } = require("./attributes");

const replaceNullWithEmptyString = (value, shouldStringify = false) => {
  return value === null
    ? ""
    : (shouldStringify && JSON.stringify(value)) || value;
};

const checkStockAvailability = (stockPassed) => {
  const result = {};
  for (const key in stockPassed) {
    result[key] = stockPassed[key] > 0;
  }
  return result;
};

module.exports.buildMetaData = (data) => {
  const {
    code,
    contracted_priced_accounts,
    crom_web_product,
    main_image,
    flag_redundant,
    is_restricted,
    publication_product,
    part,
    is_new_product,
    ean,
    number_of_sales,
    category_ids,
    contracted_product,
    product_group,
    stock,
    brand,
    customer_parts,
    price2,
    main_mpn,
    family_id,
    discount_account,
    list_price,
    custom_categories,
    primary_keyword,
    fulfillment_type,
    cat2 = "",
    cat0 = "",
    cat1 = "",
    cat3 = "",
    name = "",
    category,
    promotion,
    favourite_users,
    content_level = "",
  } = data._source;

  const transformedProduct = {
    "metadata:code": code,
    "metadata:json:contracted_priced_accounts": JSON.stringify(
      contracted_priced_accounts
    ),
    "metadata:crom_web_product": crom_web_product,
    "metadata:main_image": replaceNullWithEmptyString(main_image),
    "metadata:flag_redundant": flag_redundant,
    "metadata:is_restricted": is_restricted,
    "metadata:publication_product": publication_product,
    "metadata:part": part,
    "metadata:ean": replaceNullWithEmptyString(ean),
    "metadata:number_of_sales": number_of_sales,
    "metadata:category_ids": category_ids.join(" | "),
    "metadata:product_group": product_group,
    "metadata:json:stock": JSON.stringify(checkStockAvailability(stock)),
    "metadata:brand": brand,
    "metadata:json:customer_parts1": replaceNullWithEmptyString(
      customer_parts?.length > 600
        ? customer_parts.slice(0, 600)
        : customer_parts,
      true
    ),
    "metadata:json:customer_parts2": replaceNullWithEmptyString(
      customer_parts?.length > 600 ? customer_parts.slice(600) : null,
      true
    ),
    "metadata:price2": price2,
    "metadata:main_mpn": main_mpn,
    "metadata:family_id": replaceNullWithEmptyString(family_id),
    "metadata:discount_account": discount_account,
    "metadata:list_price": list_price,
    "metadata:json:custom_categories": JSON.stringify(custom_categories),
    "metadata:content_level": content_level,
    "metadata:primary_keyword": primary_keyword.join(" | "),
    "metadata:fulfillment_type": fulfillment_type,
    "metadata:cat0": cat0,
    "metadata:cat1": cat1,
    "metadata:cat2": cat2,
    "metadata:cat3": cat3,
    "metadata:name": name,
    "metadata:category": category || "",
    "metadata:json:promotion": JSON.stringify(promotion),
    "metadata:json:favourite_users": JSON.stringify(favourite_users),
  };

  return transformedProduct;
};

module.exports.buildFacetData = (product, isFirstProduct) => {
  const { brand, contracted_product, is_new_product, price2 } = product._source;
  const productAttributes = product._source.attributes || [];

  let finalTransform = {};
  finalTransform = allAttributes.reduce((acc, singleBaseAttribute) => {
    const doesAttributeExist = productAttributes.find(
      (attr) => attr.label.toLowerCase() === singleBaseAttribute
    );

    if (doesAttributeExist) {
      acc[`facet:${doesAttributeExist.label.toLowerCase()}`] =
        doesAttributeExist.value;
    } else {
      acc[`facet:${singleBaseAttribute}`] = "";
    }

    return acc;
  }, {});

  return {
    "facet:Price": price2,
    "facet:Brand": brand,
    "facet:Contracted Products": contracted_product,
    "facet:New Products": is_new_product,
    ...finalTransform,
  };
};
