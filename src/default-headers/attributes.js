const attributesObject = [
  {
    attribute_label: 'drilling system',
  },
  {
    attribute_label: 'collet capacity',
  },
  {
    attribute_label: 'for head weight',
  },
  {
    attribute_label: 'disposable/reusable',
  },
  {
    attribute_label: 'fork/ring tab size',
  },
  {
    attribute_label: 'smallest thread size',
  },
  {
    attribute_label: 'vacuum cleaner type',
  },
  {
    attribute_label: 'drawer qty',
  },
  {
    attribute_label: 'nose shape',
  },
  {
    attribute_label: 'maximum square size',
  },
  {
    attribute_label: 'size range',
  },
  {
    attribute_label: 'screw material',
  },
  {
    attribute_label: 'maximum torque',
  },
  {
    attribute_label: 'skirt size',
  },
  {
    attribute_label: 'compatible pipe material',
  },
  {
    attribute_label: 'regulator type',
  },
  {
    attribute_label: 'dimensions standard',
  },
  {
    attribute_label: 'maximum bore size',
  },
  {
    attribute_label: 'number of teeth',
  },
  {
    attribute_label: 'opening capacity',
  },
  {
    attribute_label: 'flange type',
  },
  {
    attribute_label: 'cutting width',
  },
  {
    attribute_label: 'container size',
  },
  {
    attribute_label: 'body thread',
  },
  {
    attribute_label: 'number of tools',
  },
  {
    attribute_label: 'overall width/diameter',
  },
  {
    attribute_label: 'boring head shank diameter',
  },
  {
    attribute_label: 'taper shank size',
  },
  {
    attribute_label: 'for format',
  },
  {
    attribute_label: 'colour temperature',
  },
  {
    attribute_label: 'maximum operating width',
  },
  {
    attribute_label: 'saw tooth size',
  },
  {
    attribute_label: 'type of input',
  },
  {
    attribute_label: 'power supply',
  },
  {
    attribute_label: 'radius',
  },
  {
    attribute_label: 'strokes per minute',
  },
  {
    attribute_label: 'chisel cutting edge',
  },
  {
    attribute_label: 'frame style',
  },
  {
    attribute_label: 'number of cupboards',
  },
  {
    attribute_label: 'maximum inlet pressure',
  },
  {
    attribute_label: 'socket size range',
  },
  {
    attribute_label: 'drill material',
  },
  {
    attribute_label: 'jaw type',
  },
  {
    attribute_label: 'fits head size',
  },
  {
    attribute_label: 'lifting height min.',
  },
  {
    attribute_label: 'number of usb sockets',
  },
  {
    attribute_label: 'belt length',
  },
  {
    attribute_label: 'battery type',
  },
  {
    attribute_label: 'equipment type',
  },
  {
    attribute_label: 'box quantity',
  },
  {
    attribute_label: 'reach',
  },
  {
    attribute_label: 'wrench type',
  },
  {
    attribute_label: 'chipbreaker',
  },
  {
    attribute_label: 'stamp type',
  },
  {
    attribute_label: 'number of leds',
  },
  {
    attribute_label: 'tensile strength',
  },
  {
    attribute_label: 'maximum current',
  },
  {
    attribute_label: 'minimum hole diameter',
  },
  {
    attribute_label: 'tap wrench type',
  },
  {
    attribute_label: 'connector size',
  },
  {
    attribute_label: 'maximum cutting width/diameter',
  },
  {
    attribute_label: 'full load run time',
  },
  {
    attribute_label: 'needle diameter',
  },
  {
    attribute_label: 'laser class',
  },
  {
    attribute_label: 'reusability',
  },
  {
    attribute_label: 'max. measured value',
  },
  {
    attribute_label: 'safety in use',
  },
  {
    attribute_label: 'height',
  },
  {
    attribute_label: 'geometry',
  },
  {
    attribute_label: 'thread profile type',
  },
  {
    attribute_label: 'max. dc voltage',
  },
  {
    attribute_label: 'screen type',
  },
  {
    attribute_label: 'sheath material',
  },
  {
    attribute_label: 'size type',
  },
  {
    attribute_label: 'tool/supply type',
  },
  {
    attribute_label: 'shackle type',
  },
  {
    attribute_label: 'cutter diameter',
  },
  {
    attribute_label: 'usb type',
  },
  {
    attribute_label: 'tab length',
  },
  {
    attribute_label: 'ac voltage',
  },
  {
    attribute_label: 'design',
  },
  {
    attribute_label: 'outside diameter (dh)',
  },
  {
    attribute_label: 'focal length',
  },
  {
    attribute_label: 'voltage range',
  },
  {
    attribute_label: 'tip size',
  },
  {
    attribute_label: 'nib style',
  },
  {
    attribute_label: 'time delay',
  },
  {
    attribute_label: 'fixing hole centres dimensions',
  },
  {
    attribute_label: 'maximum flow rate',
  },
  {
    attribute_label: 'switching type',
  },
  {
    attribute_label: 'fold type',
  },
  {
    attribute_label: 'maximum core drilling in masonry',
  },
  {
    attribute_label: 're-recordable/one-time use',
  },
  {
    attribute_label: 'hex width',
  },
  {
    attribute_label: 'pulley style',
  },
  {
    attribute_label: 'maximum inlet air pressure',
  },
  {
    attribute_label: 'bore',
  },
  {
    attribute_label: 'individual or set',
  },
  {
    attribute_label: 'round cutting capacity at 45°',
  },
  {
    attribute_label: 'taper',
  },
  {
    attribute_label: 'breaking strain',
  },
  {
    attribute_label: 'cutting height range',
  },
  {
    attribute_label: 'swing over bed',
  },
  {
    attribute_label: 'maximum operating pressure',
  },
  {
    attribute_label: 'safety classification',
  },
  {
    attribute_label: 'inserts included',
  },
  {
    attribute_label: 'collection box capacity',
  },
  {
    attribute_label: 'price',
  },
  {
    attribute_label: 'number of shelves',
  },
  {
    attribute_label: 'insert style',
  },
  {
    attribute_label: 'style',
  },
  {
    attribute_label: 'blade operation',
  },
  {
    attribute_label: 'pad shape',
  },
  {
    attribute_label: 'number of persons',
  },
  {
    attribute_label: 'compatible shank size',
  },
  {
    attribute_label: 'product category',
  },
  {
    attribute_label: 'case type',
  },
  {
    attribute_label: 'hood style',
  },
  {
    attribute_label: 'cutter style',
  },
  {
    attribute_label: 'iso designation',
  },
  {
    attribute_label: 'rivet nut diameter',
  },
  {
    attribute_label: 'no. of wheels',
  },
  {
    attribute_label: 'part code',
  },
  {
    attribute_label: 'use',
  },
  {
    attribute_label: 'bond type',
  },
  {
    attribute_label: 'bearing type',
  },
  {
    attribute_label: 'no. of treads',
  },
  {
    attribute_label: 'step material',
  },
  {
    attribute_label: 'heat output',
  },
  {
    attribute_label: 'clamp features',
  },
  {
    attribute_label: 'pressure gauge',
  },
  {
    attribute_label: 'part type',
  },
  {
    attribute_label: 'special features',
  },
  {
    attribute_label: 'maximum grooving depth',
  },
  {
    attribute_label: 'maximum hole diameter',
  },
  {
    attribute_label: 'control type',
  },
  {
    attribute_label: 'shank width',
  },
  {
    attribute_label: 'number of ports',
  },
  {
    attribute_label: 'shape',
  },
  {
    attribute_label: 'metric or imperial',
  },
  {
    attribute_label: 'body belt',
  },
  {
    attribute_label: 'test voltage',
  },
  {
    attribute_label: 'size system',
  },
  {
    attribute_label: 'blade/head material',
  },
  {
    attribute_label: 'indicator diameter',
  },
  {
    attribute_label: 'minimum square size',
  },
  {
    attribute_label: 'chuck size',
  },
  {
    attribute_label: 'caliper type',
  },
  {
    attribute_label: 'leg length',
  },
  {
    attribute_label: 'cutter manufacturer grade',
  },
  {
    attribute_label: 'height adjustability',
  },
  {
    attribute_label: 'sterility',
  },
  {
    attribute_label: 'line regulation',
  },
  {
    attribute_label: 'plug type',
  },
  {
    attribute_label: 'port size',
  },
  {
    attribute_label: 'external height',
  },
  {
    attribute_label: 'maximum bearing weight',
  },
  {
    attribute_label: 'suitable for doors',
  },
  {
    attribute_label: 'seal width',
  },
  {
    attribute_label: 'number of treads',
  },
  {
    attribute_label: 'spanner size',
  },
  {
    attribute_label: 'metal detectable',
  },
  {
    attribute_label: 'compatible rivet nut size',
  },
  {
    attribute_label: 'cutter finish/coating',
  },
  {
    attribute_label: 'knurl pattern',
  },
  {
    attribute_label: 'screw size',
  },
  {
    attribute_label: 'key number',
  },
  {
    attribute_label: 'stroke length',
  },
  {
    attribute_label: 'maximum operating temperature',
  },
  {
    attribute_label: 'usage type',
  },
  {
    attribute_label: 'cutout diameter',
  },
  {
    attribute_label: 'thread size',
  },
  {
    attribute_label: 'fit',
  },
  {
    attribute_label: 'coating',
  },
  {
    attribute_label: 'system of measurement',
  },
  {
    attribute_label: 'flavour',
  },
  {
    attribute_label: 'reamer diameter',
  },
  {
    attribute_label: 'end mill type',
  },
  {
    attribute_label: 'operating temperature range',
  },
  {
    attribute_label: 'taper bush number',
  },
  {
    attribute_label: 'helix type',
  },
  {
    attribute_label: 'heat output settings',
  },
  {
    attribute_label: 'thread length',
  },
  {
    attribute_label: 'c',
  },
  {
    attribute_label: 'internal dimensions diameter',
  },
  {
    attribute_label: 'working pressure',
  },
  {
    attribute_label: 'ink colour',
  },
  {
    attribute_label: 'compatible connector',
  },
  {
    attribute_label: 'pitch size',
  },
  {
    attribute_label: 'shaft',
  },
  {
    attribute_label: 'backing pad diameter',
  },
  {
    attribute_label: 'threading location',
  },
  {
    attribute_label: 'power usage',
  },
  {
    attribute_label: 'blade diameter',
  },
  {
    attribute_label: 'cut size',
  },
  {
    attribute_label: 'maximum steam pressure',
  },
  {
    attribute_label: 'overall width',
  },
  {
    attribute_label: 'horsepower',
  },
  {
    attribute_label: 'beam angle',
  },
  {
    attribute_label: 'front format',
  },
  {
    attribute_label: 'recyclable/non-recyclable',
  },
  {
    attribute_label: 'number of phases',
  },
  {
    attribute_label: 'blade style',
  },
  {
    attribute_label: 'lubricant description',
  },
  {
    attribute_label: 'shaft length',
  },
  {
    attribute_label: 'micron rating',
  },
  {
    attribute_label: 'sign type',
  },
  {
    attribute_label: 'speed at 50hz',
  },
  {
    attribute_label: 'impression letter type',
  },
  {
    attribute_label: 'chaser type',
  },
  {
    attribute_label: 'stud diameter',
  },
  {
    attribute_label: 'number of pegs',
  },
  {
    attribute_label: 'compatible bit size',
  },
  {
    attribute_label: 'hose diameter',
  },
  {
    attribute_label: 'mobility',
  },
  {
    attribute_label: 'maximum cutting diameter',
  },
  {
    attribute_label: 'wire feeder',
  },
  {
    attribute_label: 'chain link type',
  },
  {
    attribute_label: 'outer diameter',
  },
  {
    attribute_label: 'flute length',
  },
  {
    attribute_label: 'wedge material',
  },
  {
    attribute_label: 'number of trays',
  },
  {
    attribute_label: 'maximum pipe thickness',
  },
  {
    attribute_label: 'contact material',
  },
  {
    attribute_label: 'rung material',
  },
  {
    attribute_label: 'heating output',
  },
  {
    attribute_label: 'unit measured',
  },
  {
    attribute_label: 'contact diameter',
  },
  {
    attribute_label: 'included angle',
  },
  {
    attribute_label: 'depth to diameter ratio',
  },
  {
    attribute_label: 'contact points',
  },
  {
    attribute_label: 'rung shape',
  },
  {
    attribute_label: 'suitable for use with',
  },
  {
    attribute_label: 'tap material',
  },
  {
    attribute_label: 'plate thickness',
  },
  {
    attribute_label: 'base type',
  },
  {
    attribute_label: 'output',
  },
  {
    attribute_label: 'accessory type',
  },
  {
    attribute_label: 'wireless connectivity',
  },
  {
    attribute_label: 'electrode type',
  },
  {
    attribute_label: 'mat material',
  },
  {
    attribute_label: 'probe length',
  },
  {
    attribute_label: 'function',
  },
  {
    attribute_label: 'open height',
  },
  {
    attribute_label: 'capacity',
  },
  {
    attribute_label: 'shackle diameter',
  },
  {
    attribute_label: 'folds',
  },
  {
    attribute_label: 'number of labels',
  },
  {
    attribute_label: 'tube capacity',
  },
  {
    attribute_label: 'character description',
  },
  {
    attribute_label: 'depth',
  },
  {
    attribute_label: 'nozzle size',
  },
  {
    attribute_label: 'size closed',
  },
  {
    attribute_label: 'adhesive material',
  },
  {
    attribute_label: 'body length',
  },
  {
    attribute_label: 'arch support',
  },
  {
    attribute_label: 'rating (amp)',
  },
  {
    attribute_label: 'model',
  },
  {
    attribute_label: 'air pressure',
  },
  {
    attribute_label: 'iso category',
  },
  {
    attribute_label: 'blanket width',
  },
  {
    attribute_label: 'pulling distance',
  },
  {
    attribute_label: 'number of rungs',
  },
  {
    attribute_label: 'pole format',
  },
  {
    attribute_label: 'fixing hole diameter',
  },
  {
    attribute_label: 'indication',
  },
  {
    attribute_label: 'measuring length',
  },
  {
    attribute_label: 'holding capacity',
  },
  {
    attribute_label: 'container type',
  },
  {
    attribute_label: 'c/w locknut',
  },
  {
    attribute_label: 'rating (amps)',
  },
  {
    attribute_label: 'length',
  },
  {
    attribute_label: 'travel',
  },
  {
    attribute_label: 'blanket length',
  },
  {
    attribute_label: 'bristle type',
  },
  {
    attribute_label: 'model/type',
  },
  {
    attribute_label: 'disc type',
  },
  {
    attribute_label: 'tap thread standard',
  },
  {
    attribute_label: 'closure type',
  },
  {
    attribute_label: 'outlet sockets rating',
  },
  {
    attribute_label: 'standard height of lift',
  },
  {
    attribute_label: 'countersink angle',
  },
  {
    attribute_label: 'max. chamfer height at 45°',
  },
  {
    attribute_label: 'coupling elements',
  },
  {
    attribute_label: 'shaft material',
  },
  {
    attribute_label: 'gate opening',
  },
  {
    attribute_label: 'number of sheets',
  },
  {
    attribute_label: 'run time',
  },
  {
    attribute_label: 'blade type',
  },
  {
    attribute_label: 'maximum pressure measurement',
  },
  {
    attribute_label: 'perforations',
  },
  {
    attribute_label: 'outside diameter',
  },
  {
    attribute_label: 'max. chamfer width at 45°',
  },
  {
    attribute_label: 'number of blades',
  },
  {
    attribute_label: 'grain size (micron)',
  },
  {
    attribute_label: 'material weight',
  },
  {
    attribute_label: 'no of tones',
  },
  {
    attribute_label: 'waist size',
  },
  {
    attribute_label: 'tool material',
  },
  {
    attribute_label: 'liner material',
  },
  {
    attribute_label: 'number of flutes',
  },
  {
    attribute_label: 'extractor arm length',
  },
  {
    attribute_label: 'base size',
  },
  {
    attribute_label: 'lead length',
  },
  {
    attribute_label: 'clamping force',
  },
  {
    attribute_label: 'wide fitting',
  },
  {
    attribute_label: 'sugar type',
  },
  {
    attribute_label: 'finish/coating',
  },
  {
    attribute_label: 'hand type',
  },
  {
    attribute_label: 'pressure gauge type',
  },
  {
    attribute_label: 'fuse size',
  },
  {
    attribute_label: 'iso/ansi reference',
  },
  {
    attribute_label: 'material thickness',
  },
  {
    attribute_label: 'type of actuation',
  },
  {
    attribute_label: 'watts',
  },
  {
    attribute_label: 'welding electrode range',
  },
  {
    attribute_label: 'nail length',
  },
  {
    attribute_label: 'input voltage',
  },
  {
    attribute_label: 'maximum weight capacity',
  },
  {
    attribute_label: 'number of tines',
  },
  {
    attribute_label: 'measuring distance',
  },
  {
    attribute_label: 'rated voltage',
  },
  {
    attribute_label: 'safe working load',
  },
  {
    attribute_label: 'cure time',
  },
  {
    attribute_label: 'output power continuous',
  },
  {
    attribute_label: 'model type',
  },
  {
    attribute_label: 'fill type',
  },
  {
    attribute_label: 'end style',
  },
  {
    attribute_label: 'contact plating',
  },
  {
    attribute_label: 'ball nose type',
  },
  {
    attribute_label: 'motor hp',
  },
  {
    attribute_label: 'thread diameter',
  },
  {
    attribute_label: 'door colour',
  },
  {
    attribute_label: 'maximum no load speed',
  },
  {
    attribute_label: 'tape length',
  },
  {
    attribute_label: 'electrode length',
  },
  {
    attribute_label: 'additional features',
  },
  {
    attribute_label: 'valved',
  },
  {
    attribute_label: 'mounting type',
  },
  {
    attribute_label: 'graduations',
  },
  {
    attribute_label: 'teeth per inch range',
  },
  {
    attribute_label: 'storage capacity',
  },
  {
    attribute_label: 'en388',
  },
  {
    attribute_label: 'outlet connection',
  },
  {
    attribute_label: 'communication type',
  },
  {
    attribute_label: 'socket size',
  },
  {
    attribute_label: 'cutting action',
  },
  {
    attribute_label: 'door style',
  },
  {
    attribute_label: 'a',
  },
  {
    attribute_label: 'detection type',
  },
  {
    attribute_label: 'tine length',
  },
  {
    attribute_label: 'terminal type',
  },
  {
    attribute_label: 'product type',
  },
  {
    attribute_label: 'number of poles',
  },
  {
    attribute_label: 'thickness',
  },
  {
    attribute_label: 'lighting effect',
  },
  {
    attribute_label: 'bolt hole centres',
  },
  {
    attribute_label: 'overall dimensions (w x d x h)',
  },
  {
    attribute_label: 'iso cut',
  },
  {
    attribute_label: 'maximum output power',
  },
  {
    attribute_label: 'maximum grooving width',
  },
  {
    attribute_label: 'main colour',
  },
  {
    attribute_label: 'alarm type',
  },
  {
    attribute_label: 'overall length (l1)',
  },
  {
    attribute_label: 'power rating',
  },
  {
    attribute_label: 'distance between centres',
  },
  {
    attribute_label: 'chain length',
  },
  {
    attribute_label: 'level type',
  },
  {
    attribute_label: 'sensor dimensions',
  },
  {
    attribute_label: 'termination method',
  },
  {
    attribute_label: 'castor type',
  },
  {
    attribute_label: 'spindle speed control',
  },
  {
    attribute_label: 'maximum rpm',
  },
  {
    attribute_label: 'base material',
  },
  {
    attribute_label: 'applications',
  },
  {
    attribute_label: 'grinder type',
  },
  {
    attribute_label: 'battery capacity',
  },
  {
    attribute_label: 'fuel tank capacity',
  },
  {
    attribute_label: 'suitable for',
  },
  {
    attribute_label: 'maximum pipe capacity',
  },
  {
    attribute_label: 'supported type',
  },
  {
    attribute_label: 'wipe width',
  },
  {
    attribute_label: 'wipe length',
  },
  {
    attribute_label: 'paddle diameter',
  },
  {
    attribute_label: 'tpi',
  },
  {
    attribute_label: 'shelf material',
  },
  {
    attribute_label: 'centre hole',
  },
  {
    attribute_label: 'cartridge shape',
  },
  {
    attribute_label: 'barrier type',
  },
  {
    attribute_label: 'wet capacity',
  },
  {
    attribute_label: 'power take-off',
  },
  {
    attribute_label: 'staple leg depth',
  },
  {
    attribute_label: 'head type',
  },
  {
    attribute_label: 'i-beam width',
  },
  {
    attribute_label: 'number of inputs',
  },
  {
    attribute_label: 'head colour',
  },
  {
    attribute_label: 'lumens / lux',
  },
  {
    attribute_label: 'external width',
  },
  {
    attribute_label: 'gas type',
  },
  {
    attribute_label: 'blows per minute',
  },
  {
    attribute_label: 'lead',
  },
  {
    attribute_label: 'paddle length',
  },
  {
    attribute_label: 'face style',
  },
  {
    attribute_label: 'features',
  },
  {
    attribute_label: 'minimum temperature',
  },
  {
    attribute_label: 'dust classification',
  },
  {
    attribute_label: 'hone length',
  },
  {
    attribute_label: 'insert shape',
  },
  {
    attribute_label: 'stem diameter',
  },
  {
    attribute_label: 'tap type',
  },
  {
    attribute_label: 'external diameter',
  },
  {
    attribute_label: 'en388 abrasion',
  },
  {
    attribute_label: 'pipe tap size',
  },
  {
    attribute_label: 'plunge depth',
  },
  {
    attribute_label: 'point angle',
  },
  {
    attribute_label: 'dispensing method',
  },
  {
    attribute_label: 'load chain diameter',
  },
  {
    attribute_label: 'volume',
  },
  {
    attribute_label: 'lens colour',
  },
  {
    attribute_label: 'spine',
  },
  {
    attribute_label: 'connection type',
  },
  {
    attribute_label: 'tube size',
  },
  {
    attribute_label: 'meter dimensions',
  },
  {
    attribute_label: 'dry capacity',
  },
  {
    attribute_label: 'amp rating',
  },
  {
    attribute_label: 'maximum outlet pressure',
  },
  {
    attribute_label: 'sign message',
  },
  {
    attribute_label: 'mode of operation',
  },
  {
    attribute_label: 'strength',
  },
  {
    attribute_label: 'number of oil fins',
  },
  {
    attribute_label: 'alternative number',
  },
  {
    attribute_label: 'diameter min - max',
  },
  {
    attribute_label: 'maximum load capacity',
  },
  {
    attribute_label: 'drum capacity',
  },
  {
    attribute_label: 'head length',
  },
  {
    attribute_label: 'ruling',
  },
  {
    attribute_label: 'type of output',
  },
  {
    attribute_label: 'inside morse taper',
  },
  {
    attribute_label: 'tip size (mm)',
  },
  {
    attribute_label: 'thread limit',
  },
  {
    attribute_label: 'maximum voltage sensed',
  },
  {
    attribute_label: 'internal width',
  },
  {
    attribute_label: 'pitch',
  },
  {
    attribute_label: 'package quantity',
  },
  {
    attribute_label: 'toolholder application',
  },
  {
    attribute_label: 'maximum voltage',
  },
  {
    attribute_label: 'grit',
  },
  {
    attribute_label: 'tip size (inch)',
  },
  {
    attribute_label: 'approach angle',
  },
  {
    attribute_label: 'button marking',
  },
  {
    attribute_label: 'chisel type',
  },
  {
    attribute_label: 'maximum particle diameter',
  },
  {
    attribute_label: 'shank',
  },
  {
    attribute_label: 'external depth',
  },
  {
    attribute_label: 'camera lens size',
  },
  {
    attribute_label: 'cylinder action',
  },
  {
    attribute_label: 'compatible chain type',
  },
  {
    attribute_label: 'paper size',
  },
  {
    attribute_label: 'roll width',
  },
  {
    attribute_label: 'available operation modes',
  },
  {
    attribute_label: 'bulb fitting',
  },
  {
    attribute_label: 'air consumption',
  },
  {
    attribute_label: 'chain reference number',
  },
  {
    attribute_label: 'manufacturer guarantee',
  },
  {
    attribute_label: 'wire diameter (dd)',
  },
  {
    attribute_label: 'work bed adjustable height',
  },
  {
    attribute_label: 'wheel diameter',
  },
  {
    attribute_label: 'steel drilling capacity',
  },
  {
    attribute_label: 'drive size',
  },
  {
    attribute_label: 'for cable size',
  },
  {
    attribute_label: 'voltage',
  },
  {
    attribute_label: 'minimum pressure measurement',
  },
  {
    attribute_label: 'maximum number of padlocks',
  },
  {
    attribute_label: 'maximum chain pitch',
  },
  {
    attribute_label: 'standard lift',
  },
  {
    attribute_label: 'overall thickness',
  },
  {
    attribute_label: 'maximum impact energy',
  },
  {
    attribute_label: 'motor power',
  },
  {
    attribute_label: 'max. torque rating',
  },
  {
    attribute_label: 'form',
  },
  {
    attribute_label: 'width',
  },
  {
    attribute_label: 'spindle fitting',
  },
  {
    attribute_label: 'tank depth',
  },
  {
    attribute_label: 'corrosion resistance',
  },
  {
    attribute_label: 'acceptable quality level (aql)',
  },
  {
    attribute_label: 'action type',
  },
  {
    attribute_label: 'maximum discharge time',
  },
  {
    attribute_label: 'drill diameter inch',
  },
  {
    attribute_label: 'legend/symbol',
  },
  {
    attribute_label: 'weight (kg)',
  },
  {
    attribute_label: 'measurement accuracy',
  },
  {
    attribute_label: 'body diameter',
  },
  {
    attribute_label: 'food safe',
  },
  {
    attribute_label: 'neck type',
  },
  {
    attribute_label: 'display',
  },
  {
    attribute_label: 'product range',
  },
  {
    attribute_label: 'drill diameter letter',
  },
  {
    attribute_label: 'material',
  },
  {
    attribute_label: 'number of positions',
  },
  {
    attribute_label: 'plunge type',
  },
  {
    attribute_label: 'grinding wheel diameter',
  },
  {
    attribute_label: 'extinguishing agent',
  },
  {
    attribute_label: 'drying / bonding time',
  },
  {
    attribute_label: 'peak amps',
  },
  {
    attribute_label: 'dia.',
  },
  {
    attribute_label: 'nosepiece type',
  },
  {
    attribute_label: 'head weight',
  },
  {
    attribute_label: 'bore type',
  },
  {
    attribute_label: 'bs number',
  },
  {
    attribute_label: 'wire rope diameter',
  },
  {
    attribute_label: 'drill diameter metric',
  },
  {
    attribute_label: 'drill diameter gauge',
  },
  {
    attribute_label: 'maximum load',
  },
  {
    attribute_label: 'planing width',
  },
  {
    attribute_label: 'diameter x depth',
  },
  {
    attribute_label: 'crank force',
  },
  {
    attribute_label: 'headstrap type',
  },
  {
    attribute_label: 'hand preference',
  },
  {
    attribute_label: 'outside size',
  },
  {
    attribute_label: 'filter type',
  },
  {
    attribute_label: 'maximum rebating depth',
  },
  {
    attribute_label: 'graduation',
  },
  {
    attribute_label: 'slip resistance rating',
  },
  {
    attribute_label: 'block material',
  },
  {
    attribute_label: 'plug size',
  },
  {
    attribute_label: 'shaft size',
  },
  {
    attribute_label: 'lifting capacity upper rope layer',
  },
  {
    attribute_label: 'nozzles supplied',
  },
  {
    attribute_label: 'compatible diehead size',
  },
  {
    attribute_label: 'hardness',
  },
  {
    attribute_label: 'compatible toolholder type',
  },
  {
    attribute_label: 'depth x diameter ratio',
  },
  {
    attribute_label: 'indoor/outdoor usage',
  },
  {
    attribute_label: 'gun/can type',
  },
  {
    attribute_label: 'max response time',
  },
  {
    attribute_label: 'jaw width',
  },
  {
    attribute_label: 'to release morse taper',
  },
  {
    attribute_label: 'voltage rating dc',
  },
  {
    attribute_label: 'lift per crank revolution',
  },
  {
    attribute_label: 'maximum operating speed',
  },
  {
    attribute_label: 'flute type',
  },
  {
    attribute_label: 'duplicate/triplicate',
  },
  {
    attribute_label: 'compatible arbor',
  },
  {
    attribute_label: 'min. breaking load (wire rope)',
  },
  {
    attribute_label: 'maximum aluminium cutting capacity',
  },
  {
    attribute_label: 'jaw',
  },
  {
    attribute_label: 'pack quantity',
  },
  {
    attribute_label: 'staple leg width',
  },
  {
    attribute_label: 'useable rope length',
  },
  {
    attribute_label: 'shank width (b)',
  },
  {
    attribute_label: 'box / pack quantity',
  },
  {
    attribute_label: 'speed features',
  },
  {
    attribute_label: 'assigned protection factor (apf)',
  },
  {
    attribute_label: 'holder type',
  },
  {
    attribute_label: 'end shape',
  },
  {
    attribute_label: 'handle colour',
  },
  {
    attribute_label: 'video format',
  },
  {
    attribute_label: 'bristle length',
  },
  {
    attribute_label: 'tank height',
  },
  {
    attribute_label: 'tank width',
  },
  {
    attribute_label: 'choker hitch capacity',
  },
  {
    attribute_label: 'shank diameter',
  },
  {
    attribute_label: 'active ingredients',
  },
  {
    attribute_label: 'brush size',
  },
  {
    attribute_label: 'number of nozzles',
  },
  {
    attribute_label: 'enclosure',
  },
  {
    attribute_label: 'tooth configuration',
  },
  {
    attribute_label: 'type of hose clip',
  },
  {
    attribute_label: 'standards met',
  },
  {
    attribute_label: 'nozzle type',
  },
  {
    attribute_label: 'detectability',
  },
  {
    attribute_label: 'head material',
  },
  {
    attribute_label: 'platform height',
  },
  {
    attribute_label: 'insect control type',
  },
  {
    attribute_label: 'beams',
  },
  {
    attribute_label: 'en388 tear',
  },
  {
    attribute_label: 'tool type',
  },
  {
    attribute_label: 'monitoring control',
  },
  {
    attribute_label: 'warm up time',
  },
  {
    attribute_label: 'number of lasers',
  },
  {
    attribute_label: 'targeted insects',
  },
  {
    attribute_label: 'maximum cable diameter',
  },
  {
    attribute_label: 'handle style',
  },
  {
    attribute_label: 'large diameter',
  },
  {
    attribute_label: 'face hardness',
  },
  {
    attribute_label: 'helmet colour',
  },
  {
    attribute_label: 'fuel type',
  },
  {
    attribute_label: 'overall material',
  },
  {
    attribute_label: 'dispenser material',
  },
  {
    attribute_label: 'finish/colour',
  },
  {
    attribute_label: 'belt section',
  },
  {
    attribute_label: 'material technical',
  },
  {
    attribute_label: 'handle cover material',
  },
  {
    attribute_label: 'number of steps',
  },
  {
    attribute_label: 'revolutions per minute',
  },
  {
    attribute_label: 'number of ply',
  },
  {
    attribute_label: 'number of needles',
  },
  {
    attribute_label: 'compatible fluid',
  },
  {
    attribute_label: 'brush rotation speed',
  },
  {
    attribute_label: 'point material',
  },
  {
    attribute_label: 'flute construction',
  },
  {
    attribute_label: 'number of taps',
  },
  {
    attribute_label: 'magnetic pull',
  },
  {
    attribute_label: 'power type',
  },
  {
    attribute_label: 'thread',
  },
  {
    attribute_label: 'powder style',
  },
  {
    attribute_label: 'retractability',
  },
  {
    attribute_label: 'pad adhesion type',
  },
  {
    attribute_label: 'cutting style',
  },
  {
    attribute_label: 'key type',
  },
  {
    attribute_label: 'pad type',
  },
  {
    attribute_label: 'cutting application',
  },
  {
    attribute_label: 'for use with',
  },
  {
    attribute_label: 'interior diameter',
  },
  {
    attribute_label: 'b',
  },
  {
    attribute_label: 'number of drawers',
  },
  {
    attribute_label: 'sheets per roll/box',
  },
  {
    attribute_label: 'end angle type',
  },
  {
    attribute_label: 'cutting capacity',
  },
  {
    attribute_label: 'chuck type',
  },
  {
    attribute_label: 'exterior diameter',
  },
  {
    attribute_label: 'reading',
  },
  {
    attribute_label: 'airflow',
  },
  {
    attribute_label: 'pack type',
  },
  {
    attribute_label: 'maximum pressure',
  },
  {
    attribute_label: 'compatible holder',
  },
  {
    attribute_label: 'kit type',
  },
  {
    attribute_label: 'supply voltage',
  },
  {
    attribute_label: 'maximum general drilling capacity',
  },
  {
    attribute_label: 'band width',
  },
  {
    attribute_label: 'tip type',
  },
  {
    attribute_label: 'model series',
  },
  {
    attribute_label: 'wattage',
  },
  {
    attribute_label: 'overall diameter',
  },
  {
    attribute_label: 'lead size',
  },
  {
    attribute_label: 'body material',
  },
  {
    attribute_label: 'dial diameter',
  },
  {
    attribute_label: 'levelling type',
  },
  {
    attribute_label: 'number of holes',
  },
  {
    attribute_label: 'round cutting capacity at 90°',
  },
  {
    attribute_label: 'suitable paint types',
  },
  {
    attribute_label: 'weight per 100',
  },
  {
    attribute_label: 'gauge',
  },
  {
    attribute_label: 'grinding wheel width',
  },
  {
    attribute_label: 'knurl series',
  },
  {
    attribute_label: 'contains alcohol',
  },
  {
    attribute_label: 'spindle taper',
  },
  {
    attribute_label: 'minimum pipe capacity',
  },
  {
    attribute_label: 'body dia.',
  },
  {
    attribute_label: 'maximum setting time',
  },
  {
    attribute_label: 'reach type',
  },
  {
    attribute_label: 'lamp holder type',
  },
  {
    attribute_label: 'channel height',
  },
  {
    attribute_label: 'head height',
  },
  {
    attribute_label: 'face width',
  },
  {
    attribute_label: 'mask rating',
  },
  {
    attribute_label: 'fuel cells',
  },
  {
    attribute_label: 'mfr part no.',
  },
  {
    attribute_label: 'grip style',
  },
  {
    attribute_label: 'pressure application',
  },
  {
    attribute_label: 'number of vials',
  },
  {
    attribute_label: 'matting type',
  },
  {
    attribute_label: 'print colour',
  },
  {
    attribute_label: 'turning method',
  },
  {
    attribute_label: 'plug shape',
  },
  {
    attribute_label: 'en388 cut',
  },
  {
    attribute_label: 'lockable',
  },
  {
    attribute_label: 'shank diameter (d)',
  },
  {
    attribute_label: 'leading standard',
  },
  {
    attribute_label: 'chalk capacity',
  },
  {
    attribute_label: 'power',
  },
  {
    attribute_label: 'working range',
  },
  {
    attribute_label: 'tape thickness',
  },
  {
    attribute_label: 'adapter size',
  },
  {
    attribute_label: 'protection type',
  },
  {
    attribute_label: 'for torch number',
  },
  {
    attribute_label: 'input voltage/amps',
  },
  {
    attribute_label: 'groove profile',
  },
  {
    attribute_label: 'graduation type',
  },
  {
    attribute_label: 'set size',
  },
  {
    attribute_label: 'power source',
  },
  {
    attribute_label: 'phase',
  },
  {
    attribute_label: 'skirt length',
  },
  {
    attribute_label: 'sound',
  },
  {
    attribute_label: 'wedge shape',
  },
  {
    attribute_label: 'voltage rating',
  },
  {
    attribute_label: 'drill bit type',
  },
  {
    attribute_label: 'max. dc current',
  },
  {
    attribute_label: 'hose fitting',
  },
  {
    attribute_label: 'maximum steam temperature',
  },
  {
    attribute_label: 'number of beams',
  },
  {
    attribute_label: 'drive type',
  },
  {
    attribute_label: 'accuracy',
  },
  {
    attribute_label: 'average quantity',
  },
  {
    attribute_label: 'number of doors',
  },
  {
    attribute_label: 'rated current',
  },
  {
    attribute_label: 'pad diameter',
  },
  {
    attribute_label: 'number of pockets',
  },
  {
    attribute_label: 'charging time',
  },
  {
    attribute_label: 'item',
  },
  {
    attribute_label: 'to suit',
  },
  {
    attribute_label: 'portability',
  },
  {
    attribute_label: 'type of cut',
  },
  {
    attribute_label: 'sensor type',
  },
  {
    attribute_label: 'end type',
  },
  {
    attribute_label: 'bin capacity',
  },
  {
    attribute_label: 'hose connection size',
  },
  {
    attribute_label: 'processor',
  },
  {
    attribute_label: 'resistance features',
  },
  {
    attribute_label: 'strap material',
  },
  {
    attribute_label: 'case quantity',
  },
  {
    attribute_label: 'compatible knurl diameter',
  },
  {
    attribute_label: 'rated output',
  },
  {
    attribute_label: 'lock type',
  },
  {
    attribute_label: 'reset method',
  },
  {
    attribute_label: 'lifting height max.',
  },
  {
    attribute_label: 'nap size (pile length)',
  },
  {
    attribute_label: 'nlgi grade',
  },
  {
    attribute_label: 'vial type',
  },
  {
    attribute_label: 'sheath colour',
  },
  {
    attribute_label: 'compatible tap size',
  },
  {
    attribute_label: 'point diameter',
  },
  {
    attribute_label: 'pipe capacity',
  },
  {
    attribute_label: 'worktop finish',
  },
  {
    attribute_label: 'compatible head shank diameter',
  },
  {
    attribute_label: 'gauge connection location',
  },
  {
    attribute_label: 'pulley type',
  },
  {
    attribute_label: 'strap width',
  },
  {
    attribute_label: 'compatible materials',
  },
  {
    attribute_label: 'maximum pressure capacity',
  },
  {
    attribute_label: 'belt width',
  },
  {
    attribute_label: 'start method',
  },
  {
    attribute_label: 'motor',
  },
  {
    attribute_label: 'minimum grooving width',
  },
  {
    attribute_label: 'cable diameter',
  },
  {
    attribute_label: 'door width',
  },
  {
    attribute_label: 'fixing hole centres length',
  },
  {
    attribute_label: 'continuous rating',
  },
  {
    attribute_label: 'material grade',
  },
  {
    attribute_label: 'drill size',
  },
  {
    attribute_label: 'number of pins',
  },
  {
    attribute_label: 'number of outputs',
  },
  {
    attribute_label: 'temperature range',
  },
  {
    attribute_label: 'shape reference',
  },
  {
    attribute_label: 'rechargeable',
  },
  {
    attribute_label: 'volume capacity',
  },
  {
    attribute_label: 'lubricant type',
  },
  {
    attribute_label: 'number of speeds',
  },
  {
    attribute_label: 'rectangular cutting capacity',
  },
  {
    attribute_label: 'micron',
  },
  {
    attribute_label: 'ear plug type',
  },
  {
    attribute_label: 'rated capacity',
  },
  {
    attribute_label: 'number of batteries supplied',
  },
  {
    attribute_label: 'frame size',
  },
  {
    attribute_label: 'backing material',
  },
  {
    attribute_label: 'cross section',
  },
  {
    attribute_label: 'quantity',
  },
  {
    attribute_label: 'number of sections',
  },
  {
    attribute_label: 'pincer type',
  },
  {
    attribute_label: 'drill diameter',
  },
  {
    attribute_label: 'sole material',
  },
  {
    attribute_label: 'filtration rating',
  },
  {
    attribute_label: 'compatible screen size',
  },
  {
    attribute_label: 'strap length',
  },
  {
    attribute_label: 'modular toolholder',
  },
  {
    attribute_label: 'pocket dimensions',
  },
  {
    attribute_label: 'cuff style',
  },
  {
    attribute_label: 'noise level',
  },
  {
    attribute_label: 'body shape',
  },
  {
    attribute_label: 'hook type',
  },
  {
    attribute_label: 'no load speed',
  },
  {
    attribute_label: 'port location',
  },
  {
    attribute_label: 'number of heat settings',
  },
  {
    attribute_label: 'energy class',
  },
  {
    attribute_label: 't-handle adjustability',
  },
  {
    attribute_label: 'en388 puncture',
  },
  {
    attribute_label: 'hole diameter',
  },
  {
    attribute_label: 'angle',
  },
  {
    attribute_label: 'sprocket type',
  },
  {
    attribute_label: 'lockout type',
  },
  {
    attribute_label: 'weight capacity',
  },
  {
    attribute_label: 'screwdriver type',
  },
  {
    attribute_label: 'number of spindles',
  },
  {
    attribute_label: 'internal or external use',
  },
  {
    attribute_label: 'for use on',
  },
  {
    attribute_label: 'model number',
  },
  {
    attribute_label: 'compatible head weight',
  },
  {
    attribute_label: 'radius type',
  },
  {
    attribute_label: 'maximum cutting depth',
  },
  {
    attribute_label: 'maximum detection depth',
  },
  {
    attribute_label: 'energy paediatric',
  },
  {
    attribute_label: 'fire door usage',
  },
  {
    attribute_label: 'operation type',
  },
  {
    attribute_label: 'maximum tap size',
  },
  {
    attribute_label: 'overall height',
  },
  {
    attribute_label: 'minimum breaking strength (mbs)',
  },
  {
    attribute_label: 'gauge type',
  },
  {
    attribute_label: 'bowl capacity',
  },
  {
    attribute_label: 'manufacturer grade',
  },
  {
    attribute_label: 'ring colour',
  },
  {
    attribute_label: 'mirror diameter',
  },
  {
    attribute_label: 'to fit paper size',
  },
  {
    attribute_label: 'ladder type',
  },
  {
    attribute_label: 'nut type',
  },
  {
    attribute_label: 'cord/band style',
  },
  {
    attribute_label: 'maximum capacity',
  },
  {
    attribute_label: 'voltage rating ac',
  },
  {
    attribute_label: 'helix angle',
  },
  {
    attribute_label: 'coating coverage',
  },
  {
    attribute_label: 'can material',
  },
  {
    attribute_label: 'chamfer',
  },
  {
    attribute_label: 'angle of view',
  },
  {
    attribute_label: 'anvil diameter',
  },
  {
    attribute_label: 'compatable drive measurement',
  },
  {
    attribute_label: 'handle length',
  },
  {
    attribute_label: 'compatible heat gun brand',
  },
  {
    attribute_label: 'edge type',
  },
  {
    attribute_label: 'drive shape',
  },
  {
    attribute_label: 'maximum temperature',
  },
  {
    attribute_label: 'plug colour',
  },
  {
    attribute_label: 'deburring application',
  },
  {
    attribute_label: 'spigot size',
  },
  {
    attribute_label: 'maximum water temperature',
  },
  {
    attribute_label: 'single or double end',
  },
  {
    attribute_label: 'number of pieces',
  },
  {
    attribute_label: 'units of measurement',
  },
  {
    attribute_label: 'maximum paper size',
  },
  {
    attribute_label: 'body style',
  },
  {
    attribute_label: 'internal dimensions depth',
  },
  {
    attribute_label: 'compatible toolholder iso designation',
  },
  {
    attribute_label: 'nipple type',
  },
  {
    attribute_label: 'surfaces',
  },
  {
    attribute_label: 'centres',
  },
  {
    attribute_label: 'head width',
  },
  {
    attribute_label: 'number of grooves',
  },
  {
    attribute_label: 'overall exterior width',
  },
  {
    attribute_label: 'insulation measurement',
  },
  {
    attribute_label: 'trip characteristics',
  },
  {
    attribute_label: 'includes',
  },
  {
    attribute_label: 'cloth type',
  },
  {
    attribute_label: 'maximum drilling depth',
  },
  {
    attribute_label: 'number of stations',
  },
  {
    attribute_label: 'braille',
  },
  {
    attribute_label: 'minimum bore diameter (d)',
  },
  {
    attribute_label: 'minimum bore size',
  },
  {
    attribute_label: 'corner radius',
  },
  {
    attribute_label: 'roller diameter (d)',
  },
  {
    attribute_label: 'visibility features',
  },
  {
    attribute_label: 'plug material',
  },
  {
    attribute_label: 'bit material',
  },
  {
    attribute_label: 'spacing',
  },
  {
    attribute_label: 'side slot type',
  },
  {
    attribute_label: 'tape colour',
  },
  {
    attribute_label: 'pull force',
  },
  {
    attribute_label: 'welding current range',
  },
  {
    attribute_label: 'thread sizes male',
  },
  {
    attribute_label: 'distance between inner plates (e)',
  },
  {
    attribute_label: 'surface treatment',
  },
  {
    attribute_label: 'measurement standard',
  },
  {
    attribute_label: 'nom. hole diameter',
  },
  {
    attribute_label: 'pipette type',
  },
  {
    attribute_label: 'minimum chain pitch',
  },
  {
    attribute_label: 'overall length',
  },
  {
    attribute_label: 'cutter number',
  },
  {
    attribute_label: 'number of functions',
  },
  {
    attribute_label: 'sleeve material',
  },
  {
    attribute_label: 'head size',
  },
  {
    attribute_label: 'morse taper',
  },
  {
    attribute_label: 'brush type',
  },
  {
    attribute_label: 'pin diameter',
  },
  {
    attribute_label: 'maximum head',
  },
  {
    attribute_label: 'supported shape',
  },
  {
    attribute_label: 'en388 cut, coup test',
  },
  {
    attribute_label: 'housing material',
  },
  {
    attribute_label: 'frame colour',
  },
  {
    attribute_label: 'usage time',
  },
  {
    attribute_label: 'head diameter',
  },
  {
    attribute_label: 'lip type',
  },
  {
    attribute_label: 'max. ac voltage',
  },
  {
    attribute_label: 'ac/dc current',
  },
  {
    attribute_label: 'handle shape',
  },
  {
    attribute_label: 'sharpener material',
  },
  {
    attribute_label: 'dimensions w x d x h (mm)',
  },
  {
    attribute_label: 'taper bush',
  },
  {
    attribute_label: 'movement and adjustability',
  },
  {
    attribute_label: 'extension range',
  },
  {
    attribute_label: 'bending method',
  },
  {
    attribute_label: 'operating system',
  },
  {
    attribute_label: 'max. push force',
  },
  {
    attribute_label: 'number of pages',
  },
  {
    attribute_label: 'tap extractor sizes',
  },
  {
    attribute_label: 'number of ways',
  },
  {
    attribute_label: 'material form',
  },
  {
    attribute_label: 'overall length/head size',
  },
  {
    attribute_label: 'specification',
  },
  {
    attribute_label: 'flange width',
  },
  {
    attribute_label: 'ladder material',
  },
  {
    attribute_label: 'compatible manufacturer part number',
  },
  {
    attribute_label: 'wall movement',
  },
  {
    attribute_label: 'body orientation',
  },
  {
    attribute_label: 'labels per pack',
  },
  {
    attribute_label: 'jaw reach',
  },
  {
    attribute_label: 'test cycle',
  },
  {
    attribute_label: 'inner diameter',
  },
  {
    attribute_label: 'folded height',
  },
  {
    attribute_label: 'minimum bore diameter',
  },
  {
    attribute_label: 'face diameter',
  },
  {
    attribute_label: 'drill length type',
  },
  {
    attribute_label: 'number of sockets',
  },
  {
    attribute_label: 'maximum bend angle',
  },
  {
    attribute_label: 'insert size',
  },
  {
    attribute_label: 'torque range',
  },
  {
    attribute_label: 'series',
  },
  {
    attribute_label: 'pump type',
  },
  {
    attribute_label: 'output sockets',
  },
  {
    attribute_label: 'categories',
  },
  {
    attribute_label: 'chest size',
  },
  {
    attribute_label: 'thread direction',
  },
  {
    attribute_label: 'tool number',
  },
  {
    attribute_label: 'connectivity type',
  },
  {
    attribute_label: 'channel width',
  },
  {
    attribute_label: 'temperature resistance range',
  },
  {
    attribute_label: 'trade size',
  },
  {
    attribute_label: 'chuck diameter',
  },
  {
    attribute_label: 'sheet material',
  },
  {
    attribute_label: 'vents',
  },
  {
    attribute_label: 'shaft type',
  },
  {
    attribute_label: 'clamp type',
  },
  {
    attribute_label: 'wire sizes',
  },
  {
    attribute_label: 'maximum working capacity',
  },
  {
    attribute_label: 'for nom. size',
  },
  {
    attribute_label: 'insert length',
  },
  {
    attribute_label: 'brushed or brushless',
  },
  {
    attribute_label: 'minimum tap size',
  },
  {
    attribute_label: 'core size',
  },
  {
    attribute_label: 'resolution',
  },
  {
    attribute_label: 'nozzle diameter',
  },
  {
    attribute_label: 'brand',
  },
  {
    attribute_label: 'minimum bore diameter (d)',
  },
  {
    attribute_label: 'number of strands',
  },
  {
    attribute_label: 'lamp type',
  },
  {
    attribute_label: 'memory',
  },
  {
    attribute_label: 'maximum cutting capacity',
  },
  {
    attribute_label: 'inStock',
  },
  {
    attribute_label: 'compatible insert',
  },
  {
    attribute_label: 'die type',
  },
  {
    attribute_label: 'number of bulbs',
  },
  {
    attribute_label: 'message',
  },
  {
    attribute_label: 'cabinet style',
  },
  {
    attribute_label: 'pressure range',
  },
  {
    attribute_label: 'insulated',
  },
  {
    attribute_label: 'chain size',
  },
  {
    attribute_label: 'chuck capacity',
  },
  {
    attribute_label: 'shaft diameter',
  },
  {
    attribute_label: 'dispenser capacity',
  },
  {
    attribute_label: 'battery life',
  },
  {
    attribute_label: 'energy rating',
  },
  {
    attribute_label: 'lumens',
  },
  {
    attribute_label: 'set type',
  },
  {
    attribute_label: 'square width',
  },
  {
    attribute_label: 'brush diameter',
  },
  {
    attribute_label: 'en388 cut, tdm test iso 13997',
  },
  {
    attribute_label: 'cartridge size',
  },
  {
    attribute_label: 'cabinet door type',
  },
  {
    attribute_label: 'number of ejectors',
  },
  {
    attribute_label: 'grip material',
  },
  {
    attribute_label: 'working length',
  },
  {
    attribute_label: 'electrode size',
  },
  {
    attribute_label: 'shank shape',
  },
  {
    attribute_label: 'shoe size',
  },
  {
    attribute_label: 'grade',
  },
  {
    attribute_label: 'mitre angle',
  },
  {
    attribute_label: 'switch configuration',
  },
  {
    attribute_label: 'slot width',
  },
  {
    attribute_label: 'cores',
  },
  {
    attribute_label: 'body colour',
  },
  {
    attribute_label: 'swab material',
  },
  {
    attribute_label: 'class rating',
  },
  {
    attribute_label: 'cut length',
  },
  {
    attribute_label: 'socket type',
  },
  {
    attribute_label: 'maximum drilling capacity in steel',
  },
  {
    attribute_label: 'ear defender type',
  },
  {
    attribute_label: 'insert holding type',
  },
  {
    attribute_label: 'tooth spacing',
  },
  {
    attribute_label: 'maximum heat output',
  },
  {
    attribute_label: 'arbor shank diameter',
  },
  {
    attribute_label: 'size extended',
  },
  {
    attribute_label: 'focus',
  },
  {
    attribute_label: 'speed',
  },
  {
    attribute_label: 'labels per sheet',
  },
  {
    attribute_label: 'harness points',
  },
  {
    attribute_label: 'liner type',
  },
  {
    attribute_label: 'suitable materials',
  },
  {
    attribute_label: 'efficiency rating',
  },
  {
    attribute_label: 'reamer type',
  },
  {
    attribute_label: 'gear ratio',
  },
  {
    attribute_label: 'internal depth',
  },
  {
    attribute_label: 'type',
  },
  {
    attribute_label: 'tape type',
  },
  {
    attribute_label: 'cooling type',
  },
  {
    attribute_label: 'compatible nut/bolt thread size',
  },
  {
    attribute_label: 'load regulation',
  },
  {
    attribute_label: 'set description',
  },
  {
    attribute_label: 'bottle type',
  },
  {
    attribute_label: 'child size',
  },
  {
    attribute_label: 'character height',
  },
  {
    attribute_label: 'number of dies',
  },
  {
    attribute_label: 'display type',
  },
  {
    attribute_label: 'radio frequency',
  },
  {
    attribute_label: 'colour',
  },
  {
    attribute_label: 'seat size',
  },
  {
    attribute_label: 'stem type',
  },
  {
    attribute_label: 'air inlet size',
  },
  {
    attribute_label: 'working width',
  },
  {
    attribute_label: 'data storage capacity',
  },
  {
    attribute_label: 'wheel material',
  },
  {
    attribute_label: 'overall depth',
  },
  {
    attribute_label: 'pressure rating (bar)',
  },
  {
    attribute_label: 'arbor hole diameter',
  },
  {
    attribute_label: 'sheet capacity',
  },
  {
    attribute_label: 'tank capacity',
  },
  {
    attribute_label: 'counterbore diameter',
  },
  {
    attribute_label: 'recording',
  },
  {
    attribute_label: 'measuring range',
  },
  {
    attribute_label: 'chalk colour',
  },
  {
    attribute_label: 'cut type',
  },
  {
    attribute_label: 'cutting diameter',
  },
  {
    attribute_label: 'frequency',
  },
  {
    attribute_label: 'working temperature',
  },
  {
    attribute_label: 'battery size',
  },
  {
    attribute_label: 'flow rate',
  },
  {
    attribute_label: 'nominal diameter',
  },
  {
    attribute_label: 'leverage',
  },
  {
    attribute_label: 'handle material',
  },
  {
    attribute_label: 'compatible knurl bore diameter',
  },
  {
    attribute_label: 'material type',
  },
  {
    attribute_label: 'tip material',
  },
  {
    attribute_label: 'blade material',
  },
  {
    attribute_label: 'maximum belt width',
  },
  {
    attribute_label: 'number of sizes',
  },
  {
    attribute_label: 'abrasive material',
  },
  {
    attribute_label: 'water capacity',
  },
  {
    attribute_label: 'cover diameter',
  },
  {
    attribute_label: 'test door mass / size',
  },
  {
    attribute_label: 'lens diameter',
  },
  {
    attribute_label: 'case material',
  },
  {
    attribute_label: 'multiplication ratio',
  },
  {
    attribute_label: 'peak style',
  },
  {
    attribute_label: 'primary colour',
  },
  {
    attribute_label: 'measures',
  },
  {
    attribute_label: 'beam colour',
  },
  {
    attribute_label: 'bore size',
  },
  {
    attribute_label: 'vice type',
  },
  {
    attribute_label: 'grooving width',
  },
  {
    attribute_label: 'thread type',
  },
  {
    attribute_label: 'screen size',
  },
  {
    attribute_label: 'rivet diameter',
  },
  {
    attribute_label: 'maximum end mill capacity',
  },
  {
    attribute_label: 'single number rating (snr)',
  },
  {
    attribute_label: 'torque measurement range',
  },
  {
    attribute_label: 'length type',
  },
  {
    attribute_label: 'compatible wire size range',
  },
  {
    attribute_label: 'fitting type',
  },
  {
    attribute_label: 'applicator type',
  },
  {
    attribute_label: 'compatable with',
  },
  {
    attribute_label: 'rope diameter',
  },
  {
    attribute_label: 'output current range',
  },
  {
    attribute_label: 'cable length',
  },
  {
    attribute_label: 'frequency range',
  },
  {
    attribute_label: 'jaw opening',
  },
  {
    attribute_label: 'neck length',
  },
  {
    attribute_label: 'tube o.d.',
  },
  {
    attribute_label: 'increments',
  },
  {
    attribute_label: 'blade finish/coating',
  },
  {
    attribute_label: 'mounting',
  },
  {
    attribute_label: 'jaw height',
  },
  {
    attribute_label: 'wedge length',
  },
  {
    attribute_label: 'tooth form',
  },
  {
    attribute_label: 'insulation type',
  },
  {
    attribute_label: 'square drive size',
  },
  {
    attribute_label: 'torque adjustability',
  },
  {
    attribute_label: 'shank thread size',
  },
  {
    attribute_label: 'light type',
  },
  {
    attribute_label: 'cutting edge length',
  },
  {
    attribute_label: 'jet size',
  },
  {
    attribute_label: 'maximum user weight',
  },
  {
    attribute_label: 'pack size',
  },
  {
    attribute_label: 'jaw material',
  },
  {
    attribute_label: 'maximum compatible wire size',
  },
  {
    attribute_label: 'compatible blade height',
  },
  {
    attribute_label: 'tooth set',
  },
  {
    attribute_label: 'point type',
  },
  {
    attribute_label: 'cutting direction',
  },
  {
    attribute_label: 'scanner type',
  },
  {
    attribute_label: 'roller length',
  },
  {
    attribute_label: 'ph range',
  },
  {
    attribute_label: 'hydraulic tool type',
  },
  {
    attribute_label: 'contact configuration',
  },
  {
    attribute_label: 'absorbent capacity',
  },
  {
    attribute_label: 'coolant delivery',
  },
  {
    attribute_label: 'weight each',
  },
  {
    attribute_label: 'max. pressure reading',
  },
  {
    attribute_label: 'maximum load voltage',
  },
  {
    attribute_label: 'lifting capacity/maximum load',
  },
  {
    attribute_label: 'pin type',
  },
  {
    attribute_label: 'suitable for case',
  },
  {
    attribute_label: 'spindle diameter',
  },
  {
    attribute_label: 'fragrance',
  },
  {
    attribute_label: 'iso category grade',
  },
  {
    attribute_label: 'inlet connection',
  },
  {
    attribute_label: 'clamping diameter',
  },
  {
    attribute_label: 'broom head width',
  },
  {
    attribute_label: 'wheel diameter (circumference)',
  },
  {
    attribute_label: 'collar size',
  },
  {
    attribute_label: 'torque wrench type',
  },
  {
    attribute_label: 'maximum shackle diameter',
  },
  {
    attribute_label: 'compatible knurl face width',
  },
  {
    attribute_label: 'number of nesting lockers',
  },
  {
    attribute_label: 'flange',
  },
  {
    attribute_label: 'gas consumption',
  },
  {
    attribute_label: 'connector type',
  },
  {
    attribute_label: 'electrical phase',
  },
  {
    attribute_label: 'number of cutting heights',
  },
  {
    attribute_label: 'display colour',
  },
  {
    attribute_label: 'blade shape',
  },
  {
    attribute_label: 'face colour',
  },
  {
    attribute_label: 'contact type',
  },
  {
    attribute_label: 'maximum drilling capacity',
  },
  {
    attribute_label: 'mirror type',
  },
  {
    attribute_label: 'print type',
  },
  {
    attribute_label: 'sleeve type',
  },
  {
    attribute_label: 'clamping capacity',
  },
  {
    attribute_label: 'phase sensitivity',
  },
  {
    attribute_label: 'manifold type',
  },
  {
    attribute_label: 'bristle shape',
  },
  {
    attribute_label: 'max load',
  },
  {
    attribute_label: 'ply',
  },
  {
    attribute_label: 'throat depth',
  },
  {
    attribute_label: 'belt type',
  },
  {
    attribute_label: 'system type',
  },
  {
    attribute_label: 'esd',
  },
  {
    attribute_label: 'number of levels',
  },
  {
    attribute_label: 'minimum voltage sensed',
  },
  {
    attribute_label: 'ventilation type',
  },
  {
    attribute_label: 'pumping capacity',
  },
  {
    attribute_label: 'brand',
  },
  {
    attribute_label: 'shank length',
  },
  {
    attribute_label: 'cutting edge angle',
  },
  {
    attribute_label: 'number of compartments',
  },
  {
    attribute_label: 'upper material',
  },
  {
    attribute_label: 'body width',
  },
  {
    attribute_label: 'blade height',
  },
  {
    attribute_label: 'hose length',
  },
  {
    attribute_label: 'profile size',
  },
  {
    attribute_label: 'driving features',
  },
  {
    attribute_label: 'mount',
  },
  {
    attribute_label: 'motor rating',
  },
  {
    attribute_label: 'boring type',
  },
  {
    attribute_label: 'profile',
  },
  {
    attribute_label: 'sheets per pack',
  },
  {
    attribute_label: 'cycle life',
  },
  {
    attribute_label: 'compatible diehead type',
  },
  {
    attribute_label: 'mixing capacity',
  },
  {
    attribute_label: 'hose bore',
  },
  {
    attribute_label: 'handle type',
  },
  {
    attribute_label: 'detected',
  },
  {
    attribute_label: 'cutter form',
  },
  {
    attribute_label: 'reel size',
  },
  {
    attribute_label: 'minimum operating temperature',
  },
  {
    attribute_label: 'finish',
  },
  {
    attribute_label: 'max. input pressure',
  },
  {
    attribute_label: 'gender',
  },
  {
    attribute_label: 'size',
  },
  {
    attribute_label: 'maximum operating height',
  },
  {
    attribute_label: 'tape width',
  },
  {
    attribute_label: 'number of knurls held',
  },
  {
    attribute_label: 'cutting material',
  },
  {
    attribute_label: 'ingress protection rating (ip rating)',
  },
  {
    attribute_label: 'bolt size',
  },
  {
    attribute_label: 'staple magazine capacity',
  },
  {
    attribute_label: 'power input',
  },
  {
    attribute_label: 'frame material',
  },
  {
    attribute_label: 'thread standard',
  },
  {
    attribute_label: 'headset type',
  },
  {
    attribute_label: 'mains frequency',
  },
  {
    attribute_label: 'coverage',
  },
  {
    attribute_label: 'square drive input',
  },
  {
    attribute_label: 'orientation',
  },
  {
    attribute_label: 'dimensions',
  },
  {
    attribute_label: 'thread pitch',
  },
  {
    attribute_label: 'charging current',
  },
  {
    attribute_label: 'load capacity',
  },
  {
    attribute_label: 'tine material',
  },
  {
    attribute_label: 'lifting capacity',
  },
  {
    attribute_label: 'carbon/non-carbon',
  },
  {
    attribute_label: 'thread sizes female',
  },
  {
    attribute_label: 'bore diameter',
  },
  {
    attribute_label: 'time segment',
  },
  {
    attribute_label: 'throat angle',
  },
  {
    attribute_label: 'locker type',
  },
  {
    attribute_label: 'maximum jaw capacity',
  },
  {
    attribute_label: 'inside diameter',
  },
  {
    attribute_label: 'shelf capacity',
  },
  {
    attribute_label: 'maximum face mill capacity',
  },
  {
    attribute_label: 'cutter width',
  },
  {
    attribute_label: 'output frequency',
  },
  {
    attribute_label: 'hook length',
  },
  {
    attribute_label: 'format',
  },
  {
    attribute_label: 'shank material',
  },
  {
    attribute_label: 'maximum jaw opening',
  },
  {
    attribute_label: 'wire diameter',
  },
  {
    attribute_label: 'dc voltage',
  },
  {
    attribute_label: 'blade length',
  },
  {
    attribute_label: 'max. ac current',
  },
  {
    attribute_label: 'key capacity',
  },
  {
    attribute_label: 'sound level',
  },
  {
    attribute_label: 'maximum volume capacity',
  },
  {
    attribute_label: 'bristle material',
  },
  {
    attribute_label: 'line length',
  },
  {
    attribute_label: 'maximum steel cutting capacity',
  },
  {
    attribute_label: 'output voltage',
  },
  {
    attribute_label: 'removes',
  },
  {
    attribute_label: 'cutting edge radius',
  },
  {
    attribute_label: 'diameter (mm)',
  },
  {
    attribute_label: 'calender year',
  },
  {
    attribute_label: 'brush width',
  },
  {
    attribute_label: 'energy adult',
  },
  {
    attribute_label: 'roller material',
  },
  {
    attribute_label: 'number of terminals',
  },
  {
    attribute_label: 'cover type',
  },
  {
    attribute_label: 'collet size',
  },
  {
    attribute_label: 'mount type',
  },
  {
    attribute_label: 'wheel type',
  },
  {
    attribute_label: 'amperage',
  },
  {
    attribute_label: 'magnification',
  },
  {
    attribute_label: 'diameter',
  },
  {
    attribute_label: 'bristle colour',
  },
  {
    attribute_label: 'overall exterior depth',
  },
  {
    attribute_label: 'max. wheel diameter',
  },
  {
    attribute_label: 'compatible spigot size',
  },
  {
    attribute_label: 'withstand temperatures up to',
  },
  {
    attribute_label: 'compatible pipe size',
  },
  {
    attribute_label: 'mop head material',
  },
  {
    attribute_label: 'current range',
  },
  {
    attribute_label: 'minimum cable diameter',
  },
  {
    attribute_label: 'blade size',
  },
  {
    attribute_label: 'shank type',
  },
  {
    attribute_label: 'mounting diameter',
  },
  {
    attribute_label: 'shank size',
  },
  {
    attribute_label: 'rcd ratings (iδn)',
  },
  {
    attribute_label: 'compatible battery type',
  },
  {
    attribute_label: 'depth of cut',
  },
  {
    attribute_label: 'maximum compatible temperature',
  },
  {
    attribute_label: 'sealing type',
  },
  {
    attribute_label: 'load capacity',
  },
  {
    attribute_label: 'durability',
  },
  {
    attribute_label: 'teeth per inch',
  },
  {
    attribute_label: 'trade letter',
  },
  {
    attribute_label: 'grain size',
  },
  {
    attribute_label: 'cartridge reference code',
  },
  {
    attribute_label: 'number of gangs',
  },
  {
    attribute_label: 'range',
  },
  {
    attribute_label: 'for wire size',
  },
  {
    attribute_label: 'number of battery ports',
  },
  {
    attribute_label: 'weight',
  },
  {
    attribute_label: 'product material',
  },
  {
    attribute_label: 'largest thread size',
  },
  {
    attribute_label: 'cutting location',
  },
  {
    attribute_label: 'surface finish',
  },
  {
    attribute_label: 'paper type',
  },
  {
    attribute_label: 'disc diameter',
  },
  {
    attribute_label: 'seat location',
  },
  {
    attribute_label: 'maximum tank capacity',
  },
  {
    attribute_label: 'square drive output',
  },
  {
    attribute_label: 'model no.',
  },
  {
    attribute_label: 'tooth pitch',
  },
  {
    attribute_label: 'shank height',
  },
  {
    attribute_label: 'cutter material',
  },
  {
    attribute_label: 'arm type',
  },
  {
    attribute_label: 'tripping mechanism',
  },
  {
    attribute_label: 'application',
  },
  {
    attribute_label: 'maximum working pressure',
  },
  {
    attribute_label: 'maximum cutter head capacity',
  },
  {
    attribute_label: 'head shape',
  },
  {
    attribute_label: 'beaker type',
  },
  {
    attribute_label: 'mop type',
  },
  {
    attribute_label: 'screen resolution',
  },
  {
    attribute_label: 'drill point diameter',
  },
  {
    attribute_label: 'envelope seal',
  },
  {
    attribute_label: 'base diameter',
  },
  {
    attribute_label: 'minimum nut capacity',
  },
  {
    attribute_label: 'supplied in',
  },
  {
    attribute_label: 'bit type',
  },
  {
    attribute_label: 'chalk bottle size',
  },
  {
    attribute_label: 'wood drilling capacity',
  },
  {
    attribute_label: 'milling application',
  },
  {
    attribute_label: 'nail diameter',
  },
  {
    attribute_label: 'cylinder capacity',
  },
  {
    attribute_label: 'brush shape',
  },
  {
    attribute_label: 'cup colour',
  },
  {
    attribute_label: 't-slots',
  },
  {
    attribute_label: 'adhesion type',
  },
  {
    attribute_label: 'overall exterior height',
  },
  {
    attribute_label: 'steam quantity',
  },
  {
    attribute_label: 'jaw style',
  },
  {
    attribute_label: 'number of channels',
  },
  {
    attribute_label: 'toe cap material',
  },
  {
    attribute_label: 'length (l0)',
  },
  {
    attribute_label: 'dectector resolution',
  },
  {
    attribute_label: 'measurement range',
  },
  {
    attribute_label: 'memory card type',
  },
  {
    attribute_label: 'protects against',
  },
  {
    attribute_label: 'tread width',
  },
  {
    attribute_label: 'fixing method',
  },
  {
    attribute_label: 'bulb type',
  },
  {
    attribute_label: 'sheet width',
  },
  {
    attribute_label: 'line diameter',
  },
  {
    attribute_label: 'maximum rating',
  },
  {
    attribute_label: 'maximum nut capacity',
  },
  {
    attribute_label: 'overload range',
  },
  {
    attribute_label: 'pack volume',
  },
  {
    attribute_label: 'pitch diameter pd',
  },
  {
    attribute_label: 'number columns of load chains',
  },
  {
    attribute_label: 'pad size',
  },
  {
    attribute_label: 'protection features',
  },
  {
    attribute_label: 'maximum speed',
  },
  {
    attribute_label: 'max. free air delivery',
  },
  {
    attribute_label: 'toolholder style',
  },
  {
    attribute_label: 'number of legs',
  },
  {
    attribute_label: 'midsole material',
  },
  {
    attribute_label: 'inside length',
  },
  {
    attribute_label: 'drying time',
  },
  {
    attribute_label: 'tab width',
  },
  {
    attribute_label: 'length of cut',
  },
  {
    attribute_label: 'melting range',
  },
  {
    attribute_label: 'package type',
  },
  {
    attribute_label: 'face material',
  },
  {
    attribute_label: 'number of contacts',
  },
  {
    attribute_label: 'blade width',
  },
  {
    attribute_label: 'current rating',
  },
  {
    attribute_label: 'section',
  },
  {
    attribute_label: 'chain material',
  },
  {
    attribute_label: 'sheet length',
  },
  {
    attribute_label: 'outlet port size',
  },
  {
    attribute_label: 'internal height',
  },
  {
    attribute_label: 'roll length',
  },
  {
    attribute_label: 'compatible pipe diameter',
  },
  {
    attribute_label: 'abrasive type',
  },
  {
    attribute_label: 'compatibility',
  },
  {
    attribute_label: 'lens material',
  },
  {
    attribute_label: 'contaminants filtered',
  },
  {
    attribute_label: 'compatable nut measurement',
  },
  {
    attribute_label: 'c/w washer',
  },
  {
    attribute_label: 'load plate/fork depth',
  },
  {
    attribute_label: 'load plate/fork width',
  },
  {
    attribute_label: 'shackle length',
  },
  {
    attribute_label: 'height raised',
  },
  {
    attribute_label: 'height lowered',
  },
  {
    attribute_label: 'compatible hose inner diameter',
  },
  {
    attribute_label: 'thread (bsp)',
  },
  {
    attribute_label: 'pipe outer diameter',
  },
  {
    attribute_label: 'weatherproof',
  },
  {
    attribute_label: 'padlock type',
  },
  {
    attribute_label: 'security rating',
  },
  {
    attribute_label: 'cone type',
  },
  {
    attribute_label: 'adhesive',
  },
  {
    attribute_label: 'strip width',
  },
  {
    attribute_label: 'en388 impact protection',
  },
];

module.exports.allAttributes = attributesObject.map(
  (item) => item.attribute_label,
);
